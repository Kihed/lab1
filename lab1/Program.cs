﻿using System;

namespace lab1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Laboratory work #1. Git");
            Console.WriteLine("Variant #3. Rainfall");
            Console.WriteLine("Author: Oksana Shibko");
            Console.WriteLine("Group: 22POIT1z");

            RainfallHandler handler = new RainfallHandler();
            handler.ReadFromFile(@$"..\..\..\data.txt");
            handler.Print(handler.Rainfalls);

            handler.Print(handler.GetDaysWithRain());
            handler.Print(handler.GetDaysByRainfallAmount());

            handler.Print(handler.ShakerSort(SortingParam.RainfallAmountAsc));
            handler.Print(handler.ShakerSort(SortingParam.RainfallTypeAsc));

            handler.Print(handler.QuickSort(SortingParam.RainfallAmountAsc));
            handler.Print(handler.QuickSort(SortingParam.RainfallTypeAsc));

            Console.WriteLine("Введите номер месяца:");
            int month = Convert.ToInt32(Console.ReadLine());
            double totalRainfallAmount = handler.GetTotalRainfallAmountByMonth(month);
            if (totalRainfallAmount > 0)
            {
                Console.WriteLine($"Количество осадков в {month} месяце: {totalRainfallAmount}");
            }

        }
    }
}
