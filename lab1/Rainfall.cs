﻿namespace lab1
{
    public struct Rainfall
    {
        public int Day { get; }
        public int Month { get; }
        public double RainfallAmount { get; }
        public string RainfallType { get; }

        public Rainfall(int day, int month, double raainfallAmount, string rainfallType)
        {
            Day = day;
            Month = month;
            RainfallAmount = raainfallAmount;
            RainfallType = rainfallType;
        }

        public override string ToString()
        {
            return $"{Day:00} {Month:00} {RainfallAmount} {RainfallType}";
        }
    }
}
