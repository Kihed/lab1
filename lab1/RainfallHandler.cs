﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace lab1
{
    public class RainfallHandler
    {
        public List<Rainfall> Rainfalls { get; }

        public RainfallHandler() 
        {
            Rainfalls = new List<Rainfall>();
        }

        public void ReadFromFile(string filename)
        {
            using (var streamReader = new StreamReader(filename))
            {
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    string[] items = line.Split(" ");
                    int day = Convert.ToInt32(items[0]);
                    int month = Convert.ToInt32(items[1]);
                    double rainfallAmount = Double.Parse(items[2].Replace('.', ','));
                    string rainfallType = "";
                    for (int i = 3; i < items.Length; i++)
                    {
                        rainfallType += items[i];
                        if (i != items.Length - 1)
                        {
                            rainfallType += " ";
                        }
                    }

                    Rainfalls.Add(new Rainfall(day, month, rainfallAmount, rainfallType));

                }
            }
        }

        public void Print(List<Rainfall> list)
        {
            foreach (Rainfall item in list)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine();
        }

        public double GetTotalRainfallAmountByMonth(int month)
        {
            if (!Rainfalls.Any(x => x.Month == month))
            {
                Console.WriteLine("Этого месяца нет");
                return 0;
            }

            double totalRainfallAmount = 0;
            foreach(Rainfall item in Rainfalls)
            {
                if (item.Month == month)
                {
                    totalRainfallAmount += item.RainfallAmount;
                }
            }

            return totalRainfallAmount;
        }

        public List<Rainfall> GetDaysWithRain()
        {
            List<Rainfall> result = new List<Rainfall>();
            foreach(Rainfall item in Rainfalls)
            {
                if (item.RainfallType == "Дождь")
                {
                    result.Add(item);
                }
            }

            return result;
        }

        public List<Rainfall> GetDaysByRainfallAmount()
        {
            List<Rainfall> result = new List<Rainfall>();
            foreach (Rainfall item in Rainfalls)
            {
                if (item.RainfallAmount < 1.5)
                {
                    result.Add(item);
                }
            }

            return result;
        }

        public List<Rainfall> ShakerSort(SortingParam param)
        {
            List<Rainfall> sortedList = new List<Rainfall>(Rainfalls);
            int left;
            int right;
            switch (param)
            {
                case SortingParam.RainfallAmountAsc:
                    left = 0;
                    right = sortedList.Count - 1;
                    while (left <= right)
                    {
                        for (var i = right; i > left; --i)
                        {
                            if (sortedList[i - 1].RainfallAmount > sortedList[i].RainfallAmount)
                            {
                                (sortedList[i - 1], sortedList[i]) = (sortedList[i], sortedList[i - 1]);
                            }
                        }

                        ++left;
                        for (int i = left; i < right; ++i)
                        {
                            if (sortedList[i].RainfallAmount > sortedList[i + 1].RainfallAmount)
                            {
                                (sortedList[i], sortedList[i + 1]) = (sortedList[i + 1], sortedList[i]);
                            }
                        }

                        --right;
                    }

                    break;
                case SortingParam.RainfallTypeAsc:
                    left = 0;
                    right = sortedList.Count - 1;
                    while (left <= right)
                    {
                        for (var i = right; i > left; --i)
                        {
                            if (sortedList[i - 1].Day > sortedList[i].Day)
                            {
                                (sortedList[i - 1], sortedList[i]) = (sortedList[i], sortedList[i - 1]);
                            }
                        }

                        ++left;
                        for (int i = left; i < right; ++i)
                        {
                            if (sortedList[i].Day > sortedList[i + 1].Day)
                            {
                                (sortedList[i], sortedList[i + 1]) = (sortedList[i + 1], sortedList[i]);
                            }
                        }

                        --right;
                    }

                    left = 0;
                    right = sortedList.Count - 1;
                    while (left <= right)
                    {
                        for (var i = right; i > left; --i)
                        {
                            if (sortedList[i - 1].Month > sortedList[i].Month)
                            {
                                (sortedList[i - 1], sortedList[i]) = (sortedList[i], sortedList[i - 1]);
                            }
                        }

                        ++left;
                        for (int i = left; i < right; ++i)
                        {
                            if (sortedList[i].Month > sortedList[i + 1].Month)
                            {
                                (sortedList[i], sortedList[i + 1]) = (sortedList[i + 1], sortedList[i]);
                            }
                        }

                        --right;
                    }

                    left = 0;
                    right = sortedList.Count - 1;
                    while (left <= right)
                    {
                        for (var i = right; i > left; --i)
                        {
                            if (sortedList[i - 1].RainfallType.CompareTo(sortedList[i].RainfallType) > 0)
                            {
                                (sortedList[i - 1], sortedList[i]) = (sortedList[i], sortedList[i - 1]);
                            }
                        }

                        ++left;
                        for (int i = left; i < right; ++i)
                        {
                            if (sortedList[i].RainfallType.CompareTo(sortedList[i + 1].RainfallType) > 0)
                            {
                                (sortedList[i], sortedList[i + 1]) = (sortedList[i + 1], sortedList[i]);
                            }
                        }

                        --right;
                    }

                    break;
            }

            return sortedList;
        }

        public List<Rainfall> QuickSort(SortingParam param)
        {
            List<Rainfall> list = new List<Rainfall>(Rainfalls);
            switch (param)
            {
                case SortingParam.RainfallAmountAsc:
                    QSort(list, 0, list.Count - 1, SortingParam.RainfallAmountAsc);
                    break;
                case SortingParam.RainfallTypeAsc:
                    QSort(list, 0, list.Count - 1, SortingParam.DayAsc);
                    QSort(list, 0, list.Count - 1, SortingParam.MonthAsc);
                    QSort(list, 0, list.Count - 1, SortingParam.RainfallTypeAsc);
                    break;
                default: return list;
            }

            return list;
        }

        private int Partition(List<Rainfall> list, int start, int end, SortingParam param)
        {
            int marker = start;
            for (int i = start; i < end; i++)
            {
                switch (param)
                {
                    case SortingParam.RainfallAmountAsc:
                        if (list[i].RainfallAmount < list[end].RainfallAmount)
                        {
                            (list[marker], list[i]) = (list[i], list[marker]);
                            marker += 1;
                        }
                        break;
                    case SortingParam.DayAsc:
                        if (list[i].Day < list[end].Day)
                        {
                            (list[marker], list[i]) = (list[i], list[marker]);
                            marker += 1;
                        }
                        break;
                    case SortingParam.MonthAsc:
                        if (list[i].Month < list[end].Month)
                        {
                            (list[marker], list[i]) = (list[i], list[marker]);
                            marker += 1;
                        }
                        break;
                    case SortingParam.RainfallTypeAsc:
                        if (list[i].RainfallType.CompareTo(list[end].RainfallType) < 0)
                        {
                            (list[marker], list[i]) = (list[i], list[marker]);
                            marker += 1;
                        }
                        break;
                }
            }

            (list[marker], list[end]) = (list[end], list[marker]);
            return marker;
        }

        private void QSort(List<Rainfall> list, int start, int end, SortingParam param)
        {
            if (start >= end)
            {
                return;
            }

            int pivot = Partition(list, start, end, param);
            QSort(list, start, pivot - 1, param);
            QSort(list, pivot + 1, end, param);

        }
    }
}
