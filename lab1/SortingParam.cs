﻿namespace lab1
{
    public enum SortingParam
    {
        RainfallAmountAsc,
        RainfallTypeAsc,
        MonthAsc,
        DayAsc
    }
}
